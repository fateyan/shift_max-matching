#include <set>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include "lib/libgraphxx/partition.h"

using namespace std;
using namespace GF;

int V, E;

struct Edge {
    int u, v;
    bool merged = false;

    Edge(int a, int b): u(a), v(b) {;}
};

Partition<int, int> P;
int PSize;
bool noV2Merge;

vector<Edge*> EL;
bool cmp_ptr(const Edge *a, const Edge *b) {
    size_t au = P.PartSize(a->u);
    size_t av = P.PartSize(a->v);
    size_t bu = P.PartSize(b->u);
    size_t bv = P.PartSize(b->v);

    if(au == bu) {
        if(av == bv) {
            if(a->u == b->u) {
                return (a->v < b->v);
            }
            return (a->u < b->u);
        }
        return (av > bv);
    }
    return (au < bu);
}

bool Input() {
    if(scanf("%d %d", &V, &E) == EOF) return false;

    for(int i = 0; i < V; ++i) {
        P.Classify(i, i);
    }
    PSize = V;

    for(int i = 0; i < E; ++i) {
        int u, v; scanf("%d %d", &u, &v);
        if(v < u) swap(u, v);
        EL.push_back(new Edge(u, v));
    }

    cerr << "Graph loaded." << endl;

    return true;
}

void PrintList() {
    puts("Edge List:");
    for(Edge* e: EL) {
        printf("\"");
        for(auto u: P.ElementsOf(e->u)) {
            printf("%d,", u);
        }
        printf("\" --- \"");
        for(auto v: P.ElementsOf(e->v)) {
            printf("%d,", v);
        }
        puts("\";");
    }

    puts("Vertex size:");
    for(int i = 0; i < V; ++i)
        printf("%d ", i);
    puts("");
    for(int i = 0; i < V; ++i)
        printf("%d ", P.PartSize(i));
    puts("");
}

void Repoint(Edge* e) {
    for(Edge* i: EL) {
        if(i == e) continue;

        if(i->u == e->v) {
            i->u = e->u;
        }
        if(i->v == e->v) {
            i->v = e->u;
        }
    }
}

void Merge(int k) {
    bool *vertexVisited = new bool[V];
    memset(vertexVisited, false, sizeof(bool) * V);

    for(Edge* e: EL) {
        if(e->u == e->v) continue;
        if(!vertexVisited[e->u] && !vertexVisited[e->v]) {
            noV2Merge = false;
            vertexVisited[e->u] = true;
            vertexVisited[e->v] = true;
            e->merged = true;
            P.MergePart(e->u, e->v);
            PSize--;

            Repoint(e);
        }
        if(PSize == k) {
            delete [] vertexVisited;
            return;
        }
    }

    delete [] vertexVisited;
}
void ReconstructEL() {
    vector<Edge*> temp = EL;
    EL.clear();
    for(Edge* e: temp) {
        if(e->merged == true) continue;
        if(e->u != e->v) {
            if(P.PartSize(e->u) > P.PartSize(e->v)) swap(e->u, e->v);
            EL.push_back(e);
        }
    }
    sort(EL.begin(), EL.end(), cmp_ptr);
}
void KPartition(int k) {
#ifdef DEBUG
    PrintList();
#endif
    int round = 1;
    do {
        noV2Merge = true;
        Merge(k);
        ReconstructEL();
#ifdef DEBUG
        printf("### Round %d\n", round++);
        PrintList();
#endif
    } while (!noV2Merge && PSize > k);

    if(noV2Merge) {
        cerr << "No vertex to merge, forced to finish." << endl;
    }

    for (const auto &i: P.Elements()) {
        printf("%d %d\n", i.first, i.second);
    }
}

void Run(int k) {
    KPartition(k);
}

int main(int argc, char **argv) {
    if (argc > 1) {
        freopen(argv[1], "r+", stdin);
        puts(argv[1]);
    }
    int k = 2;
    if (argc > 2)
        k = atoi(argv[2]);
    Input();
    Run(k);
    return 0;
}


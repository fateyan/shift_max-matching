# Usage
```bash
# xtd means eXtract TestData
make xtd && make && make test
```

# Test data
`test/extract.sh` will extract all `{fname}.zip` file into `{fname}/ `.

# Run test
`run_testdata.sh` will run `bin/mxmlmtch < test/{folders}/{fname}.in > output/{fname}.out` **IN PARALLEL**.

#ifndef __GF_GRAPH_H__
#define __GF_GRAPH_H__

#include <set>
#include <map>
#include <vector>
#include "gexception.h"
#include <cstddef>

namespace GF {

template <class VType, bool Directed = false, class WType = int>
class Graph {
protected:
    typedef const VType& VRef;

public:
    Graph(): changed(false) {}

    Graph(const Graph<VType, Directed, WType> &G):
    vids(G.vids), E(G.E), vid2idx(G.vid2idx), changed(G.changed) {
        if (!changed) {
            dis = G.dis;
            rad = G.rad;
            dia = G.dia;
            mass = G.mass;
            center = G.center;
        }
    }

public:
    virtual bool AddVertex(VRef vid) {
        if (vids.find(vid) != vids.end())
            return false;
        vid2idx[vid] = E.size();
        E.push_back(std::set<VType>());
        vids.insert(vid);
        changed = true;
        return true;
    }

    virtual bool AddEdge(VRef svid, VRef tvid) {
        const auto &s = vid2idx.find(svid);
        const auto &t = vid2idx.find(tvid);
        if (s == vid2idx.end() || t == vid2idx.end())
            throw TargetNotExistsException();
        E[s->second].insert(tvid);
        if (!Directed)
            E[t->second].insert(svid);
        return false;
    }

    virtual void Update() {
        if (!changed) return;
        // Setup adjacency matrix
        dis = std::vector< std::vector<WType> >(Order(), std::vector<WType>(Order(), static_cast<WType>(Order())));
        for (const auto &s: vid2idx) {
            dis[s.second][s.second] = 0;
            for (const auto &tvid: E[s.second]) {
                const auto &t = vid2idx.find(tvid);
                dis[s.second][t->second] = 1;
            }
        }
        // Floyd-Warshall
        for (size_t k = 0; k < dis.size(); ++k)
            for (size_t i = 0; i < dis.size(); ++i)
                for (size_t j = 0; j < dis.size(); ++j)
                    if (dis[i][j] > dis[i][k] + dis[k][j])
                        dis[i][j] = dis[i][k] + dis[k][j];
        // Calculate properties
        std::vector<WType> Ecc(Order(), 0);
        std::vector<WType> Sum(Order(), 0);
        WType minSum = Order() * Order();
        rad = Order();
        dia = 0;
        for (size_t s = 0; s < dis.size(); ++s) {
            for (size_t t = 0; t < dis.size(); ++t) {
                Ecc[s] = (Ecc[s] > dis[s][t])?(Ecc[s]):(dis[s][t]);
                Sum[s] += dis[s][t];
            }
            minSum = (minSum < Sum[s])?(minSum):(Sum[s]);
            rad = (rad < Ecc[s])?(rad):(Ecc[s]);
            dia = (dia > Ecc[s])?(dia):(Ecc[s]);
        }
        mass.clear();
        center.clear();
        for (const auto &v: vid2idx) {
            if (Ecc[v.second] == rad)
                center.insert(v.first);
            if (Sum[v.second] == minSum)
                mass.insert(v.first);
        }
        changed = false;
    }

    virtual Graph<VType, Directed, WType> Subgraph(const std::set<VType> &vset) const {
        Graph<VType, Directed, WType> result;
        for (const auto &vid: vset) {
            if (vids.find(vid) != vids.end())
                result.AddVertex(vid);
        }
        for (const auto &svid: result.V()) {
            for (const auto &tvid: N(svid)) {
                if (vset.find(tvid) != vset.end())
                    result.AddEdge(svid, tvid);
            }
        }
        return result;
    }

public:
    inline size_t Order() const { return vids.size(); }

    inline size_t Size() const {
        size_t result = 0;
        for (const auto &v: E)
            result += v.size();
        return (Directed)?(result):(result >> 1);
    }

    inline const std::set<VType>& V() const { return vids; }

    inline const std::set<VType>& N(VRef vid) const {
        const auto &v = vid2idx.find(vid);
        if (v == vid2idx.end())
            throw TargetNotExistsException();
        return E[v->second];
    }

    inline const WType& Dis(VRef svid, VRef tvid) const {
        const auto &s = vid2idx.find(svid);
        const auto &t = vid2idx.find(tvid);
        if (s == vid2idx.end() || t == vid2idx.end())
            throw TargetNotExistsException();
        return dis[s->second][t->second];
    }

    inline const std::set<VType>& Mass() const { return mass; }

    inline const std::set<VType>& Center() const { return center; }

    inline bool IsChanged() const { return changed; }

protected:
    std::set<VType> vids;
    std::map<VType, size_t> vid2idx;
    std::vector< std::set<VType> > E;
    std::vector< std::vector<WType> > dis;
    WType rad, dia;
    std::set<VType> mass, center;
    bool changed;
};

};

#endif

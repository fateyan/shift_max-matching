#include "lib/libgraphxx/graph.h"
#include "lib/libgraphxx/partition.h"
#include <iostream>
#include <fstream>
#include <string>
#include <queue>

typedef unsigned int VType;
typedef unsigned int GType;
typedef GF::Graph<VType> Graph;
typedef GF::Partition<VType, GType> Part;

Graph G;
Part P;

void ReadGraph(const char *fileName) {
    std::ifstream gin(fileName);
    std::cout << std::endl << "\"" << fileName << "\"";
    size_t n, m;
    gin >> n >> m;
    for (VType i = 0; i < n; ++i)
        G.AddVertex(i);
    for (VType i = 0; i < m; ++i) {
        VType s, t;
        gin >> s >> t;
        G.AddEdge(s, t);
    }
}

void ReadParts() {
    for (size_t i = 0; i < G.Order(); ++i) {
        VType vid; GType gid;
        std::cin >> vid >> gid;
        P.Classify(vid, gid);
    }
}

struct Bridge{
    VType src;
    GType des;
    size_t diff;
    inline bool operator > (const Bridge &cand) const { return (diff > cand.diff); }
    inline bool operator < (const Bridge &cand) const { return (diff < cand.diff); }
};

size_t SetComponent(Part &part, GType svid, GType tgid) {
    GType sgid = part.Recognize(svid);
    if (sgid == tgid)
        return 0;

    part.Classify(svid, tgid);
    size_t result = 1;
    for (const auto vid: G.N(svid)) {
        if (part.Recognize(vid) == sgid)
            result += SetComponent(part, vid, tgid);
    }
    return result;
}

bool MoveVertex(const Bridge &cand) {
    Part temp = P;
    GType sgid = temp.Recognize(cand.src);
    temp.Classify(cand.src, cand.des);
    GType tgid = G.Order();
    size_t maxSize = 0, moved = 1;
    GType maxTgid = tgid;
    for (const auto tvid: G.N(cand.src)) {
        if (temp.Recognize(tvid) == sgid) {
            size_t tmpSize = SetComponent(temp, tvid, ++tgid);
            if (maxSize < tmpSize) {
                moved += maxSize;
                maxSize = tmpSize;
                maxTgid = tgid;
            } else 
                moved += tmpSize;
        }
    }
    if (moved >= cand.diff)
        return false;
    P.Classify(cand.src, cand.des);
    for (const auto tvid: G.N(cand.src)) {
        GType tgid = temp.Recognize(tvid);
        if (tgid > G.Order() && tgid != maxTgid)
            SetComponent(P, tvid, cand.des);
    }
    return true;
}

bool Balancing() {
    std::priority_queue<Bridge> candidates;
    auto groups = P.Parts();
    std::map<GType, bool> allUnchecked;
    for (const auto &g: groups)
        allUnchecked[g] = false;
    for (const auto &s: G.V()) {
        std::map<GType, bool> visited = allUnchecked;
        const auto sgid = P.Recognize(s);
        for (const auto &t: G.N(s)) {
            const auto tgid = P.Recognize(t);
            if (visited[tgid] == false) {
                visited[tgid] = true;
                if (P.PartSize(tgid) < P.PartSize(sgid)) {
                    Bridge cand = {s, tgid, P.PartSize(sgid) - P.PartSize(tgid)};
                    candidates.push(cand);
                }
            }
        }
    }
    while (!candidates.empty()) {
        if (MoveVertex(candidates.top()))
            return true;
        candidates.pop();
    }
    return false;
}

void PrintParts(std::ostream &out) {
    for (const auto &g: P.Parts())
        out << "," << P.PartSize(g);
}

int main() {
    std::string srcGraphFile;
    std::getline(std::cin, srcGraphFile);
    ReadGraph(srcGraphFile.c_str());
    ReadParts();
    PrintParts(std::cout);
    while (Balancing()) {
#ifdef DEBUG
        PrintParts(std::cerr);
#endif
    }
    PrintParts(std::cout);
    return 0;
}

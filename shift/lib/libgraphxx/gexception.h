#ifndef __GF_GEXCEPTION_H__
#define __GF_GEXCEPTION_H__

#include <exception>

namespace GF {

class TargetNotExistsException: std::exception {};

};

#endif

#ifndef __GF_PARTITION_H__
#define __GF_PARTITION_H__

#include <set>
#include <map>
#include <vector>
#include <cstddef>
#include <algorithm>
#include "gexception.h"

namespace GF {

template <class VType, class GType>
class Partition {
protected:
    typedef const VType& VRef;
    typedef const GType& GRef;
private:
    static bool cmp(const std::pair<GType, size_t> &a, const std::pair<GType, size_t> &b) {
        return (a.second < b.second);
    }
public:
    Partition() {}
    Partition(const Partition<VType, GType> &P): part(P.part), size(P.size) {}
public:
    /**
     * Classifying the vertex(vid) into the group(pid).
     */
    void Classify(VRef vid, GRef pid) {
        auto v = part.find(vid);
        if (v != part.end()) {
            auto p = size.find(v->second);
            if (p->second == 1)
                size.erase(v->second);
            else
                --p->second;
        }
        auto p = size.find(pid);
        if (p == size.end())
            size[pid] = 1;
        else
            ++p->second;
        part[vid] = pid;
    }

    /**
     * Merging the group(pidDes) with another group(pidSrc).
     */
    void MergePart(GRef pidDes, GRef pidSrc) {
        auto p1 = size.find(pidDes);
        auto p2 = size.find(pidSrc);
        if (p1 == size.end() || p2 == size.end())
            throw TargetNotExistsException();
        for (auto &v: part) {
            if (v.second == pidSrc)
                v.second = pidDes;
        }
        size[pidDes] += size[pidSrc];
        size.erase(pidSrc);
    }

    /**
     * Recognizing which group the vertex(vid) belongs to.
     */
    GRef Recognize(VRef vid) const {
        const auto &v = part.find(vid);
        if (v == part.end())
            throw TargetNotExistsException();
        return v->second;
    }

    /**
     * Getting all the elements(or vertices) which in the group(gid).
     */
    std::set<VType> ElementsOf(GRef gid) {
        std::set<VType> result;
        for (const auto &v: part) {
            if (v.second == gid)
                result.insert(v.first);
        }
        return result;
    }

    /**
     * Getting size of the group(gid).
     */
    size_t PartSize(GRef gid) const {
        const auto &g = size.find(gid);
        return (g == size.end())?0:(g->second);
    }

    /**
     * Getting all the elements (which in all the groups).
     */
    inline const std::map<VType, GType>& Elements() const { return part; }

    /**
     * Getting groups (ordered by group size in ascending order). 
     */
    std::vector<GType> Parts() const {
        std::vector<std::pair<GType, size_t> > items;
        for (const auto &i: size)
            items.push_back(i);
        std::sort(items.begin(), items.end(), cmp);
        std::vector<GType> result;
        for (const auto &i: items)
            result.push_back(i.first);
        return result;
    }

protected:
    std::map<VType, GType> part;
    std::map<GType, size_t> size;
};

};

#endif

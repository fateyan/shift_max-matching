#include <map>
#include <tuple>
#include <memory>
#include <numeric>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include "lib/libgraphxx/partition.h"
#include "lib/libgraphxx/graph.h"

using namespace std;
using namespace GF;

int V, E;

typedef int VType;
typedef int PType; // Partition Type
Graph<VType> G;
Partition<VType, PType> P;

bool _isAP(VType v, VType parent, PType j, unique_ptr<int[]>& ti, unique_ptr<int[]>& low, int t = 0) {
    ti[v] = low[v] = ++t;

    int child = 0;
    for(auto u : G.N(v)) {
        if(u != parent) {
            if(ti[u]) {
                low[v] = min(low[v], ti[u]);
            } else {
                child++;
                _isAP(u, v, j, ti, low, t);

                low[v] = min(low[v], low[u]);
                if(low[u] >= ti[v] && parent != -1) return true;
            }
        }
    }

    if(parent == -1 && child > 1)
        return true;
    return false;
}

bool isAP(VType v, PType j) {
    unique_ptr<int[]> ti(new int[V]);
    fill(ti.get(), ti.get()+V, false);
    unique_ptr<int[]> low(new int[V]);
    fill(low.get(), low.get()+V, false);
    return _isAP(v, -1, j, ti, low);
}

int dfsCountPartition(PType j, unique_ptr<bool[]>& visited, VType u) {
    int sz = 0;
    for(auto x : G.N(u)) {
        if(P.Recognize(x) == j && visited[x] == false) {
            visited[x] = true;
            sz += 1 + dfsCountPartition(j, visited, x);
        }
    }
    return sz;
}

tuple<VType, int> weightOfCSet(PType j, VType v) {
    unique_ptr<bool[]> visited(new bool[V]);
    fill(visited.get(), visited.get()+V, false);
    visited[v] = true;
    
    vector<int> ccSize;
    int totalSize = 1; // 1 is for AP
    int maxW = -1;
    VType cc_entry = -1;
    // TODO Check singleton
    for(auto u : G.N(v)) {
        if(P.Recognize(u) == j && visited[u] == false) {
            visited[u] = true;
            auto sz = 1 + dfsCountPartition(j, visited, u);
            if(sz > maxW) {
                maxW = sz;
                cc_entry = u;
            }
            totalSize += sz;
        }
    }
    return {cc_entry, totalSize-maxW};
}

vector<VType> Border(PType i, PType j) {
    vector<VType> border;
    auto cc = P.ElementsOf(j);
    for(auto u : cc) {
        for(auto v : G.N(u)) {
            if(P.Recognize(v) == i) {
                border.push_back(u);
                break;
            }
        }
    }
    return border;
}

void _dfsClassifyCCs(PType i, PType j, VType u, unique_ptr<bool[]>& visited) {
    for(auto x : G.N(u)) {
        if(P.Recognize(x) == j && visited[x] == false) {
            visited[x] = true;
            P.Classify(x, i);
            _dfsClassifyCCs(i, j, x, visited);
        }
    }
}

int dfsClassifyCCs(PType i, PType j, VType ap, VType ccEntry) {
    unique_ptr<bool[]> visited(new bool[V]);
    fill(visited.get(), visited.get()+V, false);

    visited[ccEntry] = true;
    visited[ap] = true;
    dfsCountPartition(j, visited, ccEntry);
    P.Classify(ap, i);
    for(auto x : G.N(ap)) {
        if(P.Recognize(x) == j && visited[x] == false) {
            visited[x] = true;
            P.Classify(x, i);
            _dfsClassifyCCs(i, j, x, visited);
        }
    }
    return 0;
}

bool Input() {
    if(scanf("%d %d", &V, &E) == EOF) return false;

    for(int i = 0; i < V; ++i) {
        G.AddVertex(i);
    }

    for(int i = 0; i < E; ++i) {
        int u, v; scanf("%d %d", &u, &v);
        if(v < u) swap(u, v);
        G.AddEdge(u, v);
    }

    cerr << "Graph loaded." << endl;

    return true;
}

void KPartition(int k) {
    for(int i = 0; i < V; ++i) {
        P.Classify(i, k-1);
    }

    int lightest = 0;
    while(true) {
        int minrho = 99999;
        int minrho_i = -1;
        int minrho_j = -1;
        int minrho_v = -1;
        int minrho_cc_entry = -1;
        for(int i = 0; i < k; ++i) {
            for(int j = i+1; j < k; ++j) {
                // enumerate Cj and O
                for(auto v : Border(i, j)) {
                    if(isAP(v, j)) { // Q_ij
                        // calc weight of Cj+(v)
                        auto [cc_entry, phi] = weightOfCSet(j, v);
                        if(minrho > phi) {
                            minrho = phi;
                            minrho_i = i;
                            minrho_j = j;
                            minrho_v = v;
                            minrho_cc_entry = cc_entry;
                        }
                    } else { // O_ij
                        auto phi = 1; //weightOfOSet();
                        if(minrho > phi) {
                            minrho = phi;
                            minrho_i = i;
                            minrho_j = j;
                            minrho_v = v;
                            minrho_cc_entry = -1;
                        }
                    }
                }
            }
        }

        auto ai = static_cast<int>(P.PartSize(minrho_i));
        if(lightest > (ai - minrho))
            break;

        //P[i] = P[i].union(PartitionOf(minrho));
        //P[j] = P[j].minus(PartitionOf(maxphi));
        if(minrho_cc_entry != -1)
            dfsClassifyCCs(minrho_i, minrho_j, minrho_v, minrho_cc_entry);
        else
            P.Classify(minrho_v, minrho_i);
        lightest = P.Parts()[0]; // min { P }
    }
}

void PrintPartitions() {
    for(auto p : P.Parts()) {
        for(auto e : P.ElementsOf(p)) {
            cout << e << " " << p << endl;
        }
    }
}

void Run(int k) {
    KPartition(k);
    PrintPartitions();
}

int main(int argc, char **argv) {
    if (argc > 1) {
        freopen(argv[1], "r+", stdin);
        puts(argv[1]);
    }
    int k = 2;
    if (argc > 2)
        k = atoi(argv[2]);
    Input();
    Run(k);
    return 0;
}


#/bin/env bash

BASEDIR=$(dirname "$0")

for i in $BASEDIR/*; do
    if [[ $i =~ .*\.zip ]]; then
        folder=$(echo $i | grep -oP ".*(?=\.zip)");

        mkdir -p $folder;
        echo "Create $folder/";

        7z x $i -o$folder;
        echo "Extract $i to $folder/";
    fi
done

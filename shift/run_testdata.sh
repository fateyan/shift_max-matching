#/bin/env bash

BASEDIR=$(dirname "$0")

mkdir -p $BASEDIR/output/2;
mkdir -p $BASEDIR/output/3;
mkdir -p $BASEDIR/output/5;

mxmlmtch="$BASEDIR/bin/mxmlmtch";
balancing="$BASEDIR/bin/balancing";

for k in 2 3 5; do
    for folders in $BASEDIR/test/*; do
        if [ -d $folders ]; then
            for testdata in $folders/*.in; do {
                fname=$(basename "$testdata");
                ext="${fname##*.}";
                fname="${fname%.*}";
                outname="$BASEDIR/output/$k/$fname.out";

                echo "Running $fname...";
                $mxmlmtch $testdata $k | $balancing > $outname;
                echo "Output output/$k/$fname";
            } & done
        fi
    done
done

wait
echo "done";
